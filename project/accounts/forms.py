from django import forms

from project.accounts.models import User


class ProfileForm(forms.ModelForm):
    """
    Форма профиля в личном кабинете
    """

    class Meta:
        model = User
        fields = (
            'first_name',
            'last_name',
        )

    def update_profile(self, user):
        cd = self.cleaned_data

        user.first_name = cd['first_name']
        user.last_name = cd['last_name']
        user.save()
