from django.contrib.auth import login
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.tokens import default_token_generator
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.views.generic import FormView, RedirectView

from project.accounts.forms import ProfileForm
from project.accounts.models import User


@login_required
def logout_view(request):
    logout(request)
    return redirect('landing:landing')


class TokenAuthView(RedirectView):
    """
    Авторизация по токену
    """

    def get(self, request, *args, **kwargs):
        if request.headers['User-Agent'] == 'TelegramBot (like TwitterBot)':
            return False
        return super().get(request, *args, **kwargs)

    def get_redirect_url(self, *args, **kwargs):
        uidb64 = self.kwargs.get('uidb64')
        token = self.kwargs.get('token')

        url = reverse('landing:landing')

        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=uid)
        except(TypeError, ValueError, OverflowError, User.DoesNotExist):
            pass
        else:
            if default_token_generator.check_token(user, token):
                login(self.request, user)
                url = '/account/profile/'
            else:
                logout(self.request)

        return url


class ProfileView(LoginRequiredMixin, FormView):
    """
    Страница профиля пользователя
    """

    template_name = 'accounts/profile.html'
    form_class = ProfileForm
    success_url = '/account/profile/'

    def get_initial(self):
        initial = super().get_initial()

        user = self.request.user
        initial.update({
            'first_name': user.first_name,
            'last_name': user.last_name,
        })

        return initial

    def form_valid(self, form):
        form.update_profile(self.request.user)
        return super().form_valid(form)
