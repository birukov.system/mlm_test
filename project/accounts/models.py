from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
from imagekit.models import ImageSpecField
from pilkit.processors import ResizeToFill

from project.utils.validators import FileExtValidator


class User(AbstractUser):
    bot_id = models.CharField('Telegram ID', max_length=30, blank=True)

    objects = UserManager()
