from django.urls import path

from project.accounts import views

app_name = 'account'

urlpatterns = [
    path('logout/', views.logout_view, name='logout'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
    path('<uidb64>/<token>/', views.TokenAuthView.as_view(), name='activate'),
]
