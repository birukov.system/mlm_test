import json
from functools import reduce

from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from project.accounts.models import User


def get_payload(request):
    """Извлечение данных, приходящих на webhook"""

    raw = request.body.decode("utf-8")
    try:
        payload = json.loads(raw)
    except ValueError:
        pass
    else:
        return payload


def find_between(s, first=None, last="__"):
    """Получение подстроки"""

    start = s.index(first) + len(first) if first else 0
    try:
        end = s.index(last, start)
    except ValueError:
        return s[start:]
    else:
        return s[start:end]


def ch_payload(payload):
    """Преобразование данных в словарь"""

    result = {}
    for key, value in payload.items():
        if isinstance(value, list):
            value = value[0]
        result[key] = value
        if isinstance(value, dict):
            result[key] = ch_payload(value)
    return result


def get_value(payload, item, fallback=None):
    """
    Получение вложенных данных

    d = {'a': {'b': {'c': 1}}}
    get_value(d, 'a.b.c')
    """

    serialize_dict = ch_payload(payload)

    def getitem(serialize_dict, name):
        try:
            return serialize_dict[name]
        except (KeyError, TypeError):
            return fallback

    return reduce(getitem, item.split('.'), serialize_dict)


def get_or_create_user(payload, token):
    sender = get_value(payload, "message.from.id")

    user, created = User.objects.get_or_create(
        username=sender,
        bot_id=sender,
        defaults={
            "last_name": get_value(payload, "message.from.last_name"),
            "first_name": get_value(payload, "message.from.first_name"),
            "bot_id": sender,
            "username": sender,
        },
    )

    if created:
        password = User.objects.make_random_password()
        user.set_password(password)

    return user


def get_login_link(payload, token):
    try:
        user = User.objects.get(username=get_value(payload, "message.from.id"))
    except User.DoesNotExist:
        user = get_or_create_user(payload, token)

    # TODO: изменить когда настроено на сервер, а не на ngrok
    # current_site = Site.objects.get_current()
    # domain = current_site.domain
    domain = 'https://784ef0e58225.ngrok.io'

    u_token = default_token_generator.make_token(user),
    u_uid = urlsafe_base64_encode(force_bytes(user.pk)),

    return f'{domain}/account/{u_uid[0]}/{u_token[0]}/'
