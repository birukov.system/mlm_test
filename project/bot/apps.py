from django.apps import AppConfig


class BotConfig(AppConfig):
    name = 'project.bot'
