import json
import requests

RESURCE_URL = "https://api.telegram.org/bot"


def get_telegram_data(token, method, resurce_url=RESURCE_URL):
    resp = requests.get("{url}{token}/{method}".format(url=resurce_url, token=token, method=method))
    json_data = json.loads(resp.text)
    if json_data.get('ok'):
        return json_data.get('result')


def post_telegram_data(token, method, data, resurce_url=RESURCE_URL):
    resp = requests.post("{url}{token}/{method}".format(url=resurce_url, token=token, method=method), json=data)
    json_data = json.loads(resp.text)
    if json_data.get('ok'):
        return json_data.get('result')
