from pprint import pprint
from django.conf import settings
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View

from project.bot.bot_methods import post_telegram_data
from project.bot.utils import get_payload, get_value, get_or_create_user, get_login_link


@method_decorator(csrf_exempt, name="dispatch")
class TelegramBot(View):
    """
    WebHook для Telegram
    """

    def post(self, request, *args, **kwargs):
        payload = get_payload(request)

        if payload:
            if get_value(payload, "message.text") == '/start':
                # Подписка на бота
                get_or_create_user(payload=payload, token=settings.TELEGRAM['TOKEN'])
                post_telegram_data(
                    token=settings.TELEGRAM['TOKEN'],
                    method='sendMessage',
                    data={
                        'chat_id': get_value(payload, "message.chat.id"),
                        'text': 'Привет!',
                    }
                )

            elif get_value(payload, "message.text") == '/login':
                # Запрос ссылки на взод в лк
                login_link = get_login_link(payload=payload, token=settings.TELEGRAM['TOKEN'])
                post_telegram_data(
                    token=settings.TELEGRAM['TOKEN'],
                    method='sendMessage',
                    data={
                        'chat_id': get_value(payload, "message.chat.id"),
                        'text': login_link,
                    }
                )

            else:
                # Эхо-сообщение
                post_telegram_data(
                    token=settings.TELEGRAM['TOKEN'],
                    method='sendMessage',
                    data={
                        'chat_id': get_value(payload, "message.chat.id"),
                        'text': get_value(payload, "message.text"),
                    }
                )

        return JsonResponse({}, status=200)
