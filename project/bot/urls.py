from django.urls import path

from project.bot import views

urlpatterns = [
    path('', views.TelegramBot.as_view()),
]
