from django.views.generic import TemplateView


class LandingView(TemplateView):
    """
    Лэндинг
    """
    template_name = 'landing/index.html'
